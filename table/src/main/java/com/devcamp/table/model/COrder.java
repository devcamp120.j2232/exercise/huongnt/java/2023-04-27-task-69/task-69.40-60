package com.devcamp.table.model;

import java.util.Date;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.data.annotation.CreatedDate;

import com.fasterxml.jackson.annotation.JsonFormat;

@Entity
@Table (name = "orders")

public class COrder {
    @Id
    @GeneratedValue (strategy = GenerationType.IDENTITY)
    private long id;
    

    @Temporal(TemporalType.TIMESTAMP)
    @Column (name = "created_at", nullable = true, updatable = false)
    @CreatedDate
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date createdAt;

    @ManyToOne(fetch = FetchType.LAZY)
    //@JsonIgnore
    @JoinColumn(name = "customer_id")
    private CCustomer customer;

    @OneToOne(fetch = FetchType.LAZY, cascade =  CascadeType.ALL, mappedBy = "order")
    private CPayment payment;

    @ManyToMany(fetch = FetchType.LAZY,cascade = {CascadeType.PERSIST,CascadeType.MERGE }, mappedBy = "orders")
    private Set<CProduct> products;


    public COrder() {
    }

    public COrder(long id, Date createdAt, CCustomer customer) {
        this.id = id;
        this.createdAt = createdAt;
        this.customer = customer;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public CCustomer getCustomer() {
        return customer;
    }

    public void setCustomer(CCustomer customer) {
        this.customer = customer;
    }

    public CPayment getPayment() {
        return payment;
    }

    public void setPayment(CPayment payment) {
        this.payment = payment;
    }

    public Set<CProduct> getProducts() {
        return products;
    }

    public void setProducts(Set<CProduct> products) {
        this.products = products;
    }


    
    
}
